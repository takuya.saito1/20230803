# 公開環境
## App - Vercel
- https://20230803.vercel.app/

## Storybook - Chromatic
- https://www.chromatic.com/library?appId=64cb6752cd1d5ea80c17e0c3

## Design - Figma
- https://www.figma.com/file/BCYj8PVUT0WtXZNtL8twmy/playground?type=design&node-id=0-1&mode=design&t=i0MPRvpDp2Y6Nl3D-0

# 仕様技術
- [React](https://react.dev/)
- [Next.js](https://nextjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [Storybook](https://storybook.js.org/)
- [Chromatic](https://www.chromatic.com/)