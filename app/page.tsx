export default function Home() {
  return (
    <>
      <div className={"text-4xl text-pink-300"}>Tailwind text-pink-300</div>
      <div className={"text-4xl text-[#DF0050]"}>CAPS text-pink-300</div>
    </>
  );
}
